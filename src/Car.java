import java.io.Serializable;

public class Car implements Serializable{
	String license;
	
	public Car() {
		license="";
	}
	public Car(String license){
		super();
		this.license=license;
	}
	public String getLicense(){
		return license;
	}
	public void setLicense(String license){
		this.license=license;
	}
	
	public String toString(){
		return license;
	}

}


