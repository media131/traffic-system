import java.util.NoSuchElementException;

public class LL<T> {
	private Node<T> head;
	private Node<T> tail;
	private int x;

	public LL() {
		head = null;
		x=0;
	}

	// insert for queue (FIFO)
	public void enqueue(T data) {
		// 1. create new node
		Node<T> node = new Node<T>(data);
		// 2. move LL head
		if (head == null) {
			this.head = node;
		} else {
			this.tail.setLink(node);
		}
		// 3. move tail to new tail
		tail = node;
		x++;
	}

	public T dequeue() {
		T element = null;
		if (isEmpty()){
			return element;
			//throw new NoSuchElementException("Queue underflow");
		}			
		element = (T) this.head.getData();
		this.head = this.head.getLink();
		x--;
		return element;
	}

	public int size(){
		return x;
	}
	private boolean isEmpty() {
		return this.head == null;
	}

	// insert for stack (LIFO)
	// public void push(T data) {
	// // 1. create new node
	// Node node = new Node(data);
	// // 1.1 set the pointer to head
	// if (head != null) {
	// node.setPtr(head);
	// }
	// // 2. move LL head
	// this.head = node;
	// }

	public String toString() {
		String list = "";
		Node<T> current = this.head;
		while (current != null) {
			list += current.getData() + " <= ";
			current = current.getLink();
		}
		return list;
	}
}





