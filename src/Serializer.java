import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;

public class Serializer {
	private static Random rand = new Random();

	public static void main(String args[]) {
		Serializer serializer = new Serializer();
		serializer.serializeAddress();
	}

	public void serializeAddress() {
		Car[] carArray = new Car[1000];
		String x = "";
		for (int i = 0; i < 1000; i++) {
			int r1 = rand.nextInt(36);
			int r2 = rand.nextInt(36);
			int r3 = rand.nextInt(36);
			int r4 = rand.nextInt(36);
			int r5 = rand.nextInt(36);
			int r6 = rand.nextInt(36);
			int r7 = rand.nextInt(36);
			x = random(x, r1);
			x = random(x, r2);
			x = random(x, r3);
			x = x + "-";
			x = random(x, r4);
			x = random(x, r5);
			x = random(x, r6);
			x = random(x, r7);
			carArray[i] = new Car(x);
			x = "";
		}

		try {
			FileOutputStream fos = new FileOutputStream("address.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(carArray);
			oos.close();
			System.out.println("File saved.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private String random(String x, int r) {
		String temp = x;

		if (r == 0)
			temp = temp + "P";
		else if (r == 1)
			temp = temp + "O";
		else if (r == 2)
			temp = temp + "I";
		else if (r == 3)
			temp = temp + "U";
		else if (r == 4)
			temp = temp + "Y";
		else if (r == 5)
			temp = temp + "T";
		else if (r == 6)
			temp = temp + "R";
		else if (r == 7)
			temp = temp + "E";
		else if (r == 8)
			temp = temp + "W";
		else if (r == 9)
			temp = temp + "Q";
		else if (r == 10)
			temp = temp + "L";
		else if (r == 11)
			temp = temp + "K";
		else if (r == 12)
			temp = temp + "J";
		else if (r == 13)
			temp = temp + "H";
		else if (r == 14)
			temp = temp + "G";
		else if (r == 15)
			temp = temp + "F";
		else if (r == 16)
			temp = temp + "D";
		else if (r == 17)
			temp = temp + "S";
		else if (r == 18)
			temp = temp + "A";
		else if (r == 19)
			temp = temp + "M";
		else if (r == 20)
			temp = temp + "N";
		else if (r == 21)
			temp = temp + "B";
		else if (r == 22)
			temp = temp + "V";
		else if (r == 23)
			temp = temp + "C";
		else if (r == 24)
			temp = temp + "X";
		else if (r == 25)
			temp = temp + "Z";
		else if (r == 26)
			temp = temp + "1";
		else if (r == 27)
			temp = temp + "2";
		else if (r == 28)
			temp = temp + "3";
		else if (r == 29)
			temp = temp + "4";
		else if (r == 30)
			temp = temp + "5";
		else if (r == 31)
			temp = temp + "6";
		else if (r == 32)
			temp = temp + "7";
		else if (r == 33)
			temp = temp + "8";
		else if (r == 34)
			temp = temp + "9";
		else
			temp = temp + "0";
		return temp;
	}
}
