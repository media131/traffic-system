import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Random;
import java.util.Scanner;

public class mainDriver {
	private static Random rand = new Random();

	public static void main(String[] args) throws InterruptedException {

		Car[] temp;
		try {//reads the ser file in with the car array
			FileInputStream fin = new FileInputStream("address.ser");
			ObjectInputStream ois = new ObjectInputStream(fin);
			temp = (Car[]) ois.readObject();			
//			for (int i = 0; i < temp.length; i++) {
//				System.out.println(i + ": " + temp[i]);
//			}
//			System.out.println("File loaded.");
			ois.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			temp = null;
		}

		Scanner keyboard = new Scanner(System.in);
		boolean gl1 = true, gl2 = false;
		int i = 5, k = 0;
		LL l1 = new LL(), l2 = new LL(), l3 = new LL(), l4 = new LL();		
		
		String Visualization = 
				 "                2 1 \n"
				+"                _ _ \n"
				+"               | | |\n"
				+"               | | |\n"
				+"               | | |\n"
				+"               | |^|\n"
				+"               |||||\n"
				+"  _____________|v|_|______________  \n"
				+"3|__________<-_|   |<-____________|3\n"
				+"4|___________->|_ _|_->___________|4\n"
				+"               | |^|\n"
				+"               |||||\n"
				+"               |v| |\n"
				+"               | | |\n"
				+"               | | |\n"
				+"               | | |\n"
				+"               |_|_|\n"
				+"                2 1 \n";
		System.out.println(Visualization);
		System.out.println("Press enter to continue");
		keyboard.nextLine();

		while (l1.size() < 25 && l2.size() < 25 && l3.size() < 25 && l4.size() < 25) {// runs until one lane reaches 25 cars
			Thread.sleep(1000);// 1 second

			int r1 = rand.nextInt(3);
			int r2 = rand.nextInt(3);
			int r3 = rand.nextInt(3);
			int r4 = rand.nextInt(3);

			if (r1 == 0) {// 1/3 chance to add a car to any lane
				l1.enqueue(temp[k]);
				k++;
			}
			if (r2 == 0) {
				l2.enqueue(temp[k]);
				k++;
			}
			if (r3 == 0) {
				l3.enqueue(temp[k]);
				k++;
			}
			if (r4 == 0) {
				l4.enqueue(temp[k]);
				k++;
			}

			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("-----------------------" + k + " total cars.--------------------------");

			if ((i % 5) == 0) {// Switches traffic lights every 5 seconds
				gl1 = !gl1;
				gl2 = !gl2;
			}

			if (gl1 == true) {// displays time remaining for red light
				l1.dequeue();
				l2.dequeue();
				System.out.println("Lanes 1: Green Light");
				System.out.println("Lanes 2: Green Light");

				if ((i % 5) == 0) {
					System.out.println("Lanes 3: Red Light for 5 seconds");
				}
				if ((i % 5) == 0) {
					System.out.println("Lanes 4: Red Light for 5 seconds");
				}
				if ((i % 5) == 1) {
					System.out.println("Lanes 3: Red Light for 4 seconds");
				}
				if ((i % 5) == 1) {
					System.out.println("Lanes 4: Red Light for 4 seconds");
				}
				if ((i % 5) == 2) {
					System.out.println("Lanes 3: Red Light for 3 seconds");
				}
				if ((i % 5) == 2) {
					System.out.println("Lanes 4: Red Light for 3 seconds");
				}
				if ((i % 5) == 3) {
					System.out.println("Lanes 3: Red Light for 2 seconds");
				}
				if ((i % 5) == 3) {
					System.out.println("Lanes 4: Red Light for 2 seconds");
				}
				if ((i % 5) == 4) {
					System.out.println("Lanes 3: Red Light for 1 second");
				}
				if ((i % 5) == 4) {
					System.out.println("Lanes 4: Red Light for 1 second");
				}
				System.out.println("--------------------------------------------------------------");
			}
			if (gl2 == true) {// displays time remaining for red light
				l3.dequeue();
				l4.dequeue();
				if ((i % 5) == 0) {
					System.out.println("Lanes 1: Red Light for 5 seconds");
				}
				if ((i % 5) == 0) {
					System.out.println("Lanes 2: Red Light for 5 seconds");
				}
				if ((i % 5) == 1) {
					System.out.println("Lanes 1: Red Light for 4 seconds");
				}
				if ((i % 5) == 1) {
					System.out.println("Lanes 2: Red Light for 4 seconds");
				}
				if ((i % 5) == 2) {
					System.out.println("Lanes 1: Red Light for 3 seconds");
				}
				if ((i % 5) == 2) {
					System.out.println("Lanes 2: Red Light for 3 seconds");
				}
				if ((i % 5) == 3) {
					System.out.println("Lanes 1: Red Light for 2 seconds");
				}
				if ((i % 5) == 3) {
					System.out.println("Lanes 2: Red Light for 2 seconds");
				}
				if ((i % 5) == 4) {
					System.out.println("Lanes 1: Red Light for 1 second");
				}
				if ((i % 5) == 4) {
					System.out.println("Lanes 2: Red Light for 1 second");
				}
				System.out.println("Lanes 3: Green Light");
				System.out.println("Lanes 4: Green Light");
				System.out.println("--------------------------------------------------------------");
			}
			// show how many cars are in each lane
			System.out.println("--------------------------------------------------------------");
			System.out.println("Lane 1: " + l1 + " : " + l1.size() + " Cars.");
			System.out.println("--------------------------------------------------------------");
			System.out.println("Lane 2: " + l2 + " : " + l2.size() + " Cars.");
			System.out.println("--------------------------------------------------------------");
			System.out.println("Lane 3: " + l3 + " : " + l3.size() + " Cars.");
			System.out.println("--------------------------------------------------------------");
			System.out.println("Lane 4: " + l4 + " : " + l4.size() + " Cars.");
			System.out.println("--------------------------------------------------------------");
			i++;// seconds total
		}
	}
}












































